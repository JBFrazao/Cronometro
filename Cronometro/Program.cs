﻿using static System.Console;
using System.Timers;
using System;



namespace Cronometro
{
    class Program
    {
        static bool clear = false;

        static void Main(string[] args)
        {
            Cronometro cronometro = new Cronometro();

            bool ya = true;
            Timer refresh = new Timer();
            refresh.Interval = (100);
            refresh.Enabled = true;
            refresh.Elapsed += new ElapsedEventHandler(Refresh_Elapsed);
            refresh.Start();

            while (ya)
            {
                while (!(Console.KeyAvailable && Console.ReadKey(true).Key == ConsoleKey.Enter))
                {
                    Clear();
                    cronometro.Stop();
                    clear = true;
                    if (!cronometro.Enabled)
                    {
                        WriteLine();
                        WriteLine("\t" + cronometro.Tempo().Substring(0, 8));
                        WriteLine("\tStart");
                    }
                    while (clear)
                    {
                    }
                }
                while (!(Console.KeyAvailable && Console.ReadKey(true).Key == ConsoleKey.Enter))
                {
                    Clear();
                    cronometro.Start();
                    clear = true;
                    if (cronometro.Enabled)
                    {
                        WriteLine();
                        WriteLine("\t" + cronometro.Tempo().Substring(0, 10));
                        WriteLine("\tStop");
                    }
                    while (clear)
                    {
                    }
                }
            }
            ReadKey();
        }
        private static void Refresh_Elapsed(object sender, ElapsedEventArgs e)
        {
            clear = false;
        }

    }
   


}
