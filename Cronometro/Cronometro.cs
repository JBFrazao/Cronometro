﻿using System.Diagnostics;
using System.Timers;


namespace Cronometro
{
    class Cronometro
    {
        private bool _startstop = false;
        private Stopwatch _tempo = new Stopwatch();

        #region Construtores
        public Cronometro()
        {

        }
        #endregion

        public void Start()
        {
            if (_startstop == false)
            {
                _startstop = true;
                _tempo.Start();
            }
            
        }

        public void Stop()
        {
            if (_startstop == true)
            {
                _startstop = false;
                _tempo.Stop();
            }
            
        }

        public string Tempo()
        {
            return _tempo.Elapsed.ToString();
        }

        public bool Enabled
        {
            get { return _startstop; }
            set
            {
                _startstop = value;
            }
        }


    }
}
